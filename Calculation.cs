﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grocery_Calculator
{
    internal static class Calculation
    {
        public static double CalcTotal (double storeMileage, double total, Store store)
        {
            if (store == Store.Custom || store == Store.Restaraunt)
            {
                Console.Clear();
                Console.Write("Please enter one-way mileage to store: ");
                double input;
                double inputTip = 0.00;

                while (!double.TryParse(Console.ReadLine(), out input))
                {
                    Console.Write("Please enter a valid amount: ");
                }

                if (store == Store.Restaraunt)
                {
                    Console.Clear();
                    Console.Write("Please enter tip given: ");
                    while (!double.TryParse(Console.ReadLine(), out inputTip))
                    {
                        Console.Write("Please enter a valid amount: ");
                    }
                }
                storeMileage = ((input * 2) * .67) + inputTip; // .67 is the standard mileage rate for 2024.

            }

            return storeMileage + total;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grocery_Calculator
{
    internal class Menu
    {
        public Store MainMenu()
        {
            Console.WriteLine("Welcome to the grocery calculator!\n" +
                "Please select the store where your receipt is from:\n" +
                "1. HEB\n" +
                "2. Kroger\n" +
                "3. Aldi\n" +
                "4. Sam's Club\n" +
                "5. Other\n"+
                "6. Tip (Restaraunt)");

            int input;
            while (!int.TryParse(Console.ReadLine(), out input) || input > 6 || input < 1)
            {
                Console.Write("Please enter a valid integer: ");
            }

            Store store = input switch
            {
                1 => Store.HEB,
                2 => Store.Kroger,
                3 => Store.Aldi,
                4 => Store.Sams,
                5 => Store.Custom,
                6 => Store.Restaraunt
            };

            return store;
        }

        public double ReceiptMenu ()
        {
            Console.Clear();
            Console.Write("Please enter receipt total: ");
            double input;
            while(!double.TryParse(Console.ReadLine(), out input))
            {
                Console.Write("Please enter a valid amount: ");
            }

            return input;
        }

        public double GetItemValues(string name)
        {
            Console.Write($"Please enter values of {name}'s items, separated by spaces: ");

            string[] input = Console.ReadLine().Split(" ");
            double output = 0.00;

            for (int i = 0; i < input.Length; i++)
            {
                output += double.Parse(input[i]);
            }

            if (name == "Abinity")
            {
                return output / 2;
            }
            else
            {
                return output;
            }
        }

    }
}

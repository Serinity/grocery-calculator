﻿using Grocery_Calculator;

Menu menu = new Menu();
Dictionary<Store, double> storeAmounts = new Dictionary<Store, double> { { Store.HEB, 2.68 }, { Store.Kroger, 3.08 }, { Store.Aldi, 6.57 }, { Store.Sams, 8.04 }, { Store.Custom, 0.00 }, { Store.Restaraunt, 0.00 } };

while (true)
{
    Console.Clear();
    Store store = menu.MainMenu();

    Console.WriteLine($"Total: {Calculation.CalcTotal(storeAmounts[store], menu.ReceiptMenu(), store)}");
    Console.WriteLine($"Abinity total (add to both): {menu.GetItemValues("Abinity")}");
    Console.WriteLine($"Jacob total: {menu.GetItemValues("Jacob")}");
    Console.WriteLine("Press any key to continue");
    Console.ReadKey(true);
}



enum Store { HEB, Kroger, Aldi, Sams, Custom, Restaraunt }
